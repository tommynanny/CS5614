﻿using System.Linq;
using System.Windows.Forms;
using System.IO;
using System;
using LinqToExcel;
using System.Data;
using System.Globalization;
using System.Collections.Generic;
using System.Security.Cryptography;
using Remotion.Reflection.CodeGeneration.DynamicMethods;
using System.Drawing;

namespace Project_LINQ
{
	public partial class MainForm : Form
	{
		public Dictionary<string, TableData> TableDatas = new Dictionary<string, TableData>();
		public Dictionary<Button, TableData> ButtonTableDatas = new Dictionary<Button, TableData>();

		string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Selected.csv");

		public MainForm()
		{
			InitializeComponent();
			this.Icon = Project_LINQ.Properties.Resources.icon;


			TableDatas = new Dictionary<string, TableData>()
			{
				{"Schedule B-1", new TableData(){ TableName = "Schedule B-1", MD5 = "60bd572fdfaaabd67259c02e4109c7d0" } },
				{"Schedule B-1.1", new TableData(){ TableName = "Schedule B-1.1", MD5 = "a0217b7d917bf7074d1274546e4cdb0a" } },
				{"Schedule B-43 Inventory", new TableData(){ TableName = "Schedule B-43 Inventory", MD5 = "c7a8ce79d6b316b21ede1b3d4746e840" } },
				{"Schedule P-1(a) Employees", new TableData(){ TableName = "Schedule P-1(a) Employees", MD5 = "23833ead17e6bfd3505ec74d0b5f3f2e" } },
				{"Schedule P-1.1", new TableData(){ TableName = "Schedule P-1.1", MD5 = "8b5e1cfae808b283f4b25f13521560d5" } },
				{"Schedule P-1.2", new TableData(){ TableName = "Schedule P-1.2", MD5 = "f4d33379e03d0f17601c3ab21e7e4168" } },
				{"Schedule P-10", new TableData(){ TableName = "Schedule P-10", MD5 = "a85b9ae08a004a81b2af39e13c056340" } },
				{"Schedule P-12(a)", new TableData(){ TableName = "Schedule P-12(a)", MD5 = "ac2d7b76becf567ff208521af07a33f7" } },
				{"Schedule P-5.1", new TableData(){ TableName = "Schedule P-5.1", MD5 = "632c5f7846506aa718648a1d0a241552" } },
				{"Schedule P-5.2", new TableData(){ TableName = "Schedule P-5.2", MD5 = "d676fa8cdb594bc0361f5e1de0a3f185" } },
				{"Schedule P-6", new TableData(){ TableName = "Schedule P-6", MD5 = "491dc61bf223be63f30f02f34e8094b8" } },
				{"Schedule P-7", new TableData(){ TableName = "Schedule P-7", MD5 = "272c16ad2bdc2f821f4702e94d963771" } },
				{"DB1BCoupon", new TableData(){ TableName = "DB1BCoupon", MD5 = "00c590cb0f3d24b3ba1a77429bf00531" } },
				{"DB1BMarket", new TableData(){ TableName = "DB1BMarket", MD5 = "3442c4153e65c83c05bbf168a4458ce6" } },
				{"DB1BTicket", new TableData(){ TableName = "DB1BTicket", MD5 = "908436156f5b4eb764d1253209a5835a" } }
			};

			ButtonTableDatas = new Dictionary<Button, TableData>()
			{
				{ Browse_F41_1, TableDatas["Schedule B-1"]},
				{ Browse_F41_2, TableDatas["Schedule B-1.1"]},
				{ Browse_F41_3, TableDatas["Schedule B-43 Inventory"]},
				{ Browse_F41_4, TableDatas["Schedule P-1(a) Employees"]},
				{ Browse_F41_5, TableDatas["Schedule P-1.1"]},
				{ Browse_F41_6, TableDatas["Schedule P-1.2"]},
				{ Browse_F41_7, TableDatas["Schedule P-10"]},
				{ Browse_F41_8, TableDatas["Schedule P-12(a)"]},
				{ Browse_F41_9, TableDatas["Schedule P-5.1"]},
				{ Browse_F41_10, TableDatas["Schedule P-5.2"]},
				{ Browse_F41_11, TableDatas["Schedule P-6"]},
				{ Browse_F41_12, TableDatas["Schedule P-7"]},

				{ Browse_DB1BCoupon, TableDatas["DB1BCoupon"]},
				{ Browse_DB1BMarket, TableDatas["DB1BMarket"]},
				{ Browse_DB1BTicket, TableDatas["DB1BTicket"]},
			};


			ImportGroupBox.Controls.Cast<Control>().ToList().ForEach(x =>
			{
				if (x != null && x.GetType() == typeof(Button))
				{
					x.Text = "Import " + ButtonTableDatas[(Button)x].TableName;
					x.Click += (sender, e) =>
					{
						BrowseExcelFile(out string current);
						CheckFileValidation((Button)x, current);

						bool allset = true;
						TableDatas.Values.ToList().ForEach(v =>
						{
							if (!v.imported)
							{
								allset = false;
							}
						});

						ActionBox.Controls.Cast<Control>().ToList().ForEach(btn =>
						{
							btn.Enabled = allset;
						});
					};
				}
			});


		}

		public void Display(IQueryable<object> source) => ResultDataGridView.DataSource = source.ToList();


		public void CheckFileValidation(Button current, string fpath)
		{
			if (string.IsNullOrEmpty(fpath)) return;
			var file = new FileInfo(fpath);

			var tableData = ButtonTableDatas[current];

			if (file.Exists)
			{
				//MessageBox.Show(file.ReadableSize(), file.FullName, MessageBoxButtons.OK);
				//var fsize = file.Length;
				//double fsize_in_gb = fsize / 1048576D;
				var md5 = tableData.MD5;
				var real = GetMD5(file.FullName);

				//MessageBox.Show($"{real} \n {md5}");

				if (real != md5)
				{
					var error = new FileDataCorruptedException("Wrong md5 checksum for the desired " +
					"data file, file may be corrupted or you have chosen different data file which does " +
					"not match the program record file.");
					MessageBox.Show(error.Message, file.FullName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				else
				{
					MessageBox.Show("File validation completed! \n Now start importing, this may take a while !");
					current.BackColor = Color.Cyan;
					tableData.imported = true;
					tableData.fpath = fpath;


					tableData.content = new ExcelQueryFactory(fpath);

					if (tableData.TableName == "DB1BCoupon" ||
					tableData.TableName == "DB1BMarket" ||
					tableData.TableName == "DB1BTicket")
					{
						var excel = tableData.content;
						var query = from c in excel.Worksheet<ExcelDataWrapper>()
									where (c.Dest.Contains("IAD") || c.Origin.Contains("IAD"))
									select c;
						Display(query);
					}

					MessageBox.Show("File imported successfully!");
					return;
				}
			}
			else
			{
				var error = new FileNotFoundException();
				MessageBox.Show(error.Message, file.FullName, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

			current.BackColor = SystemColors.Control;
			tableData.imported = false;
		}

		public void BrowseExcelFile(out string fpath)
		{
			fpath = "";
			using (OpenFileDialog openFileDialog = new OpenFileDialog())
			{
				openFileDialog.InitialDirectory = "c:\\";
				openFileDialog.Filter = "All files (*.*)|*.*";
				openFileDialog.FilterIndex = 2;
				openFileDialog.RestoreDirectory = true;

				if (openFileDialog.ShowDialog() == DialogResult.OK)
				{

					fpath = openFileDialog.FileName;
				}
			}
		}

		private string GetMD5(string fp)
		{
			string result = "";
			using (var md5 = MD5.Create())
			{
				using (var stream = File.OpenRead(fp))
				{
					var hash = md5.ComputeHash(stream);
					var stringresult = BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
					result = stringresult;
				}
			}
			return result;
		}

		private void Qa_Click(object sender, EventArgs e)
		{
			var excel = TableDatas["DB1BCoupon"].content;
			var query = from c in excel.Worksheet<ExcelDataWrapper>()
						where (c.Dest.Contains("IAD") || c.Origin.Contains("IAD"))
						select c;
			Display(query);
		}

		private void Qb_Click(object sender, EventArgs e)
		{
			var excel = TableDatas["DB1BCoupon"].content;
			var query = from c in excel.Worksheet<ExcelDataWrapper>()
						where (c.Dest.Contains("IAD") || c.Origin.Contains("IAD"))
						select c;
			Display(query);
		}

		private void Qc_Click(object sender, EventArgs e)
		{
			var excel = TableDatas["DB1BCoupon"].content;
			var query = from c in excel.Worksheet<ExcelDataWrapper>()
						where (c.Dest.Contains("IAD") || c.Origin.Contains("IAD"))
						select c;
			Display(query);
		}

		private void Qd_Click(object sender, EventArgs e)
		{
			var excel = TableDatas["DB1BCoupon"].content;
			var query = from c in excel.Worksheet<ExcelDataWrapper>()
						where (c.Dest.Contains("IAD") || c.Origin.Contains("IAD"))
						select c;
			Display(query);
		}

		private void Qe_Click(object sender, EventArgs e)
		{
			var excel = TableDatas["DB1BCoupon"].content;
			var query = from c in excel.Worksheet<ExcelDataWrapper>()
						where (c.Dest.Contains("IAD") || c.Origin.Contains("IAD"))
						select c;
			Display(query);
		}

		private void Qf_Click(object sender, EventArgs e)
		{
			var excel = TableDatas["DB1BCoupon"].content;
			var query = from c in excel.Worksheet<ExcelDataWrapper>()
						where (c.Dest.Contains("IAD") || c.Origin.Contains("IAD"))
						select c;
			Display(query);
		}

		private void Qg_Click(object sender, EventArgs e)
		{
			var excel = TableDatas["DB1BCoupon"].content;
			var query = from c in excel.Worksheet<ExcelDataWrapper>()
						where (c.Dest.Contains("IAD") || c.Origin.Contains("IAD"))
						select c;
			Display(query);
		}

		private void Qh_Click(object sender, EventArgs e)
		{
			var excel = TableDatas["DB1BCoupon"].content;
			var query = from c in excel.Worksheet<ExcelDataWrapper>()
						where (c.Dest.Contains("IAD") || c.Origin.Contains("IAD"))
						select c;
			Display(query);
		}


	}

	public class FileDataCorruptedException : Exception
	{
		public FileDataCorruptedException()
		{
		}

		public FileDataCorruptedException(string message)
			: base(message)
		{
		}

		public FileDataCorruptedException(string message, Exception inner)
			: base(message, inner)
		{
		}
	}

	public static class ExtensionMethods
	{
		public static string ReadableSize(this FileInfo file)
		{
			string[] sizes = { "B", "KB", "MB", "GB", "TB" };
			double len = file.Length;
			int order = 0;
			while (len >= 1024 && order < sizes.Length - 1)
			{
				order++;
				len = len / 1024;
			}

			// Adjust the format string to your preferences. For example "{0:0.#}{1}" would
			// show a single decimal place, and no space.
			return string.Format("{0:0.##} {1}", len, sizes[order]);
		}
	}

	public class TableData
	{
		public string TableName;
		public string MD5;
		public bool imported = false;
		public string fpath;
		public ExcelQueryFactory content;
	}

}


