﻿namespace Project_LINQ
{
	partial class MainForm
	{
		/// <summary>
		/// 必需的设计器变量。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows 窗体设计器生成的代码

		/// <summary>
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.Browse_DB1BCoupon = new System.Windows.Forms.Button();
			this.Qa = new System.Windows.Forms.Button();
			this.ResultDataGridView = new System.Windows.Forms.DataGridView();
			this.OperationPanel = new System.Windows.Forms.Panel();
			this.ActionBox = new System.Windows.Forms.GroupBox();
			this.Qh = new System.Windows.Forms.Button();
			this.Qg = new System.Windows.Forms.Button();
			this.Qf = new System.Windows.Forms.Button();
			this.Qb = new System.Windows.Forms.Button();
			this.Qe = new System.Windows.Forms.Button();
			this.Qc = new System.Windows.Forms.Button();
			this.Qd = new System.Windows.Forms.Button();
			this.ImportGroupBox = new System.Windows.Forms.GroupBox();
			this.Browse_F41_12 = new System.Windows.Forms.Button();
			this.Browse_F41_11 = new System.Windows.Forms.Button();
			this.Browse_F41_10 = new System.Windows.Forms.Button();
			this.Browse_F41_9 = new System.Windows.Forms.Button();
			this.Browse_F41_8 = new System.Windows.Forms.Button();
			this.Browse_F41_7 = new System.Windows.Forms.Button();
			this.Browse_F41_6 = new System.Windows.Forms.Button();
			this.Browse_F41_5 = new System.Windows.Forms.Button();
			this.Browse_F41_4 = new System.Windows.Forms.Button();
			this.Browse_F41_3 = new System.Windows.Forms.Button();
			this.Browse_F41_2 = new System.Windows.Forms.Button();
			this.Browse_F41_1 = new System.Windows.Forms.Button();
			this.Browse_DB1BTicket = new System.Windows.Forms.Button();
			this.Browse_DB1BMarket = new System.Windows.Forms.Button();
			this.DataGridViewPanel = new System.Windows.Forms.Panel();
			this.bindingSource = new System.Windows.Forms.BindingSource(this.components);
			((System.ComponentModel.ISupportInitialize)(this.ResultDataGridView)).BeginInit();
			this.OperationPanel.SuspendLayout();
			this.ActionBox.SuspendLayout();
			this.ImportGroupBox.SuspendLayout();
			this.DataGridViewPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// Browse_DB1BCoupon
			// 
			this.Browse_DB1BCoupon.Location = new System.Drawing.Point(6, 165);
			this.Browse_DB1BCoupon.Name = "Browse_DB1BCoupon";
			this.Browse_DB1BCoupon.Size = new System.Drawing.Size(160, 23);
			this.Browse_DB1BCoupon.TabIndex = 0;
			this.Browse_DB1BCoupon.Text = "Browse DB1BCoupon";
			this.Browse_DB1BCoupon.UseVisualStyleBackColor = true;
			// 
			// Qa
			// 
			this.Qa.Enabled = false;
			this.Qa.Location = new System.Drawing.Point(69, 66);
			this.Qa.Name = "Qa";
			this.Qa.Size = new System.Drawing.Size(75, 23);
			this.Qa.TabIndex = 1;
			this.Qa.Text = "Question A";
			this.Qa.UseVisualStyleBackColor = true;
			this.Qa.Click += new System.EventHandler(this.Qa_Click);
			// 
			// ResultDataGridView
			// 
			this.ResultDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.ResultDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ResultDataGridView.Location = new System.Drawing.Point(0, 0);
			this.ResultDataGridView.Name = "ResultDataGridView";
			this.ResultDataGridView.RowTemplate.Height = 23;
			this.ResultDataGridView.Size = new System.Drawing.Size(800, 351);
			this.ResultDataGridView.TabIndex = 2;
			// 
			// OperationPanel
			// 
			this.OperationPanel.Controls.Add(this.ActionBox);
			this.OperationPanel.Controls.Add(this.ImportGroupBox);
			this.OperationPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.OperationPanel.Location = new System.Drawing.Point(0, 351);
			this.OperationPanel.Name = "OperationPanel";
			this.OperationPanel.Size = new System.Drawing.Size(800, 221);
			this.OperationPanel.TabIndex = 3;
			// 
			// ActionBox
			// 
			this.ActionBox.Controls.Add(this.Qh);
			this.ActionBox.Controls.Add(this.Qg);
			this.ActionBox.Controls.Add(this.Qa);
			this.ActionBox.Controls.Add(this.Qf);
			this.ActionBox.Controls.Add(this.Qb);
			this.ActionBox.Controls.Add(this.Qe);
			this.ActionBox.Controls.Add(this.Qc);
			this.ActionBox.Controls.Add(this.Qd);
			this.ActionBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ActionBox.Location = new System.Drawing.Point(510, 0);
			this.ActionBox.Name = "ActionBox";
			this.ActionBox.Size = new System.Drawing.Size(290, 221);
			this.ActionBox.TabIndex = 12;
			this.ActionBox.TabStop = false;
			this.ActionBox.Text = "Action";
			// 
			// Qh
			// 
			this.Qh.Enabled = false;
			this.Qh.Location = new System.Drawing.Point(150, 153);
			this.Qh.Name = "Qh";
			this.Qh.Size = new System.Drawing.Size(75, 23);
			this.Qh.TabIndex = 11;
			this.Qh.Text = "Question H";
			this.Qh.UseVisualStyleBackColor = true;
			this.Qh.Click += new System.EventHandler(this.Qh_Click);
			// 
			// Qg
			// 
			this.Qg.Enabled = false;
			this.Qg.Location = new System.Drawing.Point(69, 153);
			this.Qg.Name = "Qg";
			this.Qg.Size = new System.Drawing.Size(75, 23);
			this.Qg.TabIndex = 10;
			this.Qg.Text = "Question G";
			this.Qg.UseVisualStyleBackColor = true;
			this.Qg.Click += new System.EventHandler(this.Qg_Click);
			// 
			// Qf
			// 
			this.Qf.Enabled = false;
			this.Qf.Location = new System.Drawing.Point(150, 124);
			this.Qf.Name = "Qf";
			this.Qf.Size = new System.Drawing.Size(75, 23);
			this.Qf.TabIndex = 9;
			this.Qf.Text = "Question F";
			this.Qf.UseVisualStyleBackColor = true;
			this.Qf.Click += new System.EventHandler(this.Qf_Click);
			// 
			// Qb
			// 
			this.Qb.Enabled = false;
			this.Qb.Location = new System.Drawing.Point(150, 66);
			this.Qb.Name = "Qb";
			this.Qb.Size = new System.Drawing.Size(75, 23);
			this.Qb.TabIndex = 5;
			this.Qb.Text = "Question B";
			this.Qb.UseVisualStyleBackColor = true;
			this.Qb.Click += new System.EventHandler(this.Qb_Click);
			// 
			// Qe
			// 
			this.Qe.Enabled = false;
			this.Qe.Location = new System.Drawing.Point(69, 124);
			this.Qe.Name = "Qe";
			this.Qe.Size = new System.Drawing.Size(75, 23);
			this.Qe.TabIndex = 8;
			this.Qe.Text = "Question E";
			this.Qe.UseVisualStyleBackColor = true;
			this.Qe.Click += new System.EventHandler(this.Qe_Click);
			// 
			// Qc
			// 
			this.Qc.Enabled = false;
			this.Qc.Location = new System.Drawing.Point(69, 95);
			this.Qc.Name = "Qc";
			this.Qc.Size = new System.Drawing.Size(75, 23);
			this.Qc.TabIndex = 6;
			this.Qc.Text = "Question C";
			this.Qc.UseVisualStyleBackColor = true;
			this.Qc.Click += new System.EventHandler(this.Qc_Click);
			// 
			// Qd
			// 
			this.Qd.Enabled = false;
			this.Qd.Location = new System.Drawing.Point(150, 95);
			this.Qd.Name = "Qd";
			this.Qd.Size = new System.Drawing.Size(75, 23);
			this.Qd.TabIndex = 7;
			this.Qd.Text = "Question D";
			this.Qd.UseVisualStyleBackColor = true;
			this.Qd.Click += new System.EventHandler(this.Qd_Click);
			// 
			// ImportGroupBox
			// 
			this.ImportGroupBox.Controls.Add(this.Browse_F41_12);
			this.ImportGroupBox.Controls.Add(this.Browse_F41_11);
			this.ImportGroupBox.Controls.Add(this.Browse_F41_10);
			this.ImportGroupBox.Controls.Add(this.Browse_F41_9);
			this.ImportGroupBox.Controls.Add(this.Browse_F41_8);
			this.ImportGroupBox.Controls.Add(this.Browse_F41_7);
			this.ImportGroupBox.Controls.Add(this.Browse_F41_6);
			this.ImportGroupBox.Controls.Add(this.Browse_F41_5);
			this.ImportGroupBox.Controls.Add(this.Browse_F41_4);
			this.ImportGroupBox.Controls.Add(this.Browse_F41_3);
			this.ImportGroupBox.Controls.Add(this.Browse_F41_2);
			this.ImportGroupBox.Controls.Add(this.Browse_F41_1);
			this.ImportGroupBox.Controls.Add(this.Browse_DB1BCoupon);
			this.ImportGroupBox.Controls.Add(this.Browse_DB1BTicket);
			this.ImportGroupBox.Controls.Add(this.Browse_DB1BMarket);
			this.ImportGroupBox.Dock = System.Windows.Forms.DockStyle.Left;
			this.ImportGroupBox.Location = new System.Drawing.Point(0, 0);
			this.ImportGroupBox.Margin = new System.Windows.Forms.Padding(20);
			this.ImportGroupBox.Name = "ImportGroupBox";
			this.ImportGroupBox.Padding = new System.Windows.Forms.Padding(20);
			this.ImportGroupBox.Size = new System.Drawing.Size(510, 221);
			this.ImportGroupBox.TabIndex = 4;
			this.ImportGroupBox.TabStop = false;
			this.ImportGroupBox.Text = "Import";
			// 
			// Browse_F41_12
			// 
			this.Browse_F41_12.Location = new System.Drawing.Point(338, 136);
			this.Browse_F41_12.Name = "Browse_F41_12";
			this.Browse_F41_12.Size = new System.Drawing.Size(160, 23);
			this.Browse_F41_12.TabIndex = 15;
			this.Browse_F41_12.Text = "Browse Schedule P-7";
			this.Browse_F41_12.UseVisualStyleBackColor = true;
			// 
			// Browse_F41_11
			// 
			this.Browse_F41_11.Location = new System.Drawing.Point(172, 136);
			this.Browse_F41_11.Name = "Browse_F41_11";
			this.Browse_F41_11.Size = new System.Drawing.Size(160, 23);
			this.Browse_F41_11.TabIndex = 14;
			this.Browse_F41_11.Text = "Browse Schedule P-6";
			this.Browse_F41_11.UseVisualStyleBackColor = true;
			// 
			// Browse_F41_10
			// 
			this.Browse_F41_10.Location = new System.Drawing.Point(6, 136);
			this.Browse_F41_10.Name = "Browse_F41_10";
			this.Browse_F41_10.Size = new System.Drawing.Size(160, 23);
			this.Browse_F41_10.TabIndex = 13;
			this.Browse_F41_10.Text = "Browse Schedule P-5.2";
			this.Browse_F41_10.UseVisualStyleBackColor = true;
			// 
			// Browse_F41_9
			// 
			this.Browse_F41_9.Location = new System.Drawing.Point(338, 107);
			this.Browse_F41_9.Name = "Browse_F41_9";
			this.Browse_F41_9.Size = new System.Drawing.Size(160, 23);
			this.Browse_F41_9.TabIndex = 12;
			this.Browse_F41_9.Text = "Browse Schedule P-5.1";
			this.Browse_F41_9.UseVisualStyleBackColor = true;
			// 
			// Browse_F41_8
			// 
			this.Browse_F41_8.Location = new System.Drawing.Point(172, 107);
			this.Browse_F41_8.Name = "Browse_F41_8";
			this.Browse_F41_8.Size = new System.Drawing.Size(160, 23);
			this.Browse_F41_8.TabIndex = 11;
			this.Browse_F41_8.Text = "Browse Schedule P-12(a)";
			this.Browse_F41_8.UseVisualStyleBackColor = true;
			// 
			// Browse_F41_7
			// 
			this.Browse_F41_7.Location = new System.Drawing.Point(6, 107);
			this.Browse_F41_7.Name = "Browse_F41_7";
			this.Browse_F41_7.Size = new System.Drawing.Size(160, 23);
			this.Browse_F41_7.TabIndex = 10;
			this.Browse_F41_7.Text = "Browse Schedule P-10";
			this.Browse_F41_7.UseVisualStyleBackColor = true;
			// 
			// Browse_F41_6
			// 
			this.Browse_F41_6.Location = new System.Drawing.Point(338, 78);
			this.Browse_F41_6.Name = "Browse_F41_6";
			this.Browse_F41_6.Size = new System.Drawing.Size(160, 23);
			this.Browse_F41_6.TabIndex = 9;
			this.Browse_F41_6.Text = "Browse Schedule P-1.2";
			this.Browse_F41_6.UseVisualStyleBackColor = true;
			// 
			// Browse_F41_5
			// 
			this.Browse_F41_5.Location = new System.Drawing.Point(172, 78);
			this.Browse_F41_5.Name = "Browse_F41_5";
			this.Browse_F41_5.Size = new System.Drawing.Size(160, 23);
			this.Browse_F41_5.TabIndex = 8;
			this.Browse_F41_5.Text = "Browse Schedule P-1.1";
			this.Browse_F41_5.UseVisualStyleBackColor = true;
			// 
			// Browse_F41_4
			// 
			this.Browse_F41_4.Location = new System.Drawing.Point(6, 78);
			this.Browse_F41_4.Name = "Browse_F41_4";
			this.Browse_F41_4.Size = new System.Drawing.Size(160, 23);
			this.Browse_F41_4.TabIndex = 7;
			this.Browse_F41_4.Text = "Browse Schedule P-1(a) Employees";
			this.Browse_F41_4.UseVisualStyleBackColor = true;
			// 
			// Browse_F41_3
			// 
			this.Browse_F41_3.Location = new System.Drawing.Point(338, 49);
			this.Browse_F41_3.Name = "Browse_F41_3";
			this.Browse_F41_3.Size = new System.Drawing.Size(160, 23);
			this.Browse_F41_3.TabIndex = 6;
			this.Browse_F41_3.Text = "Browse Schedule B-43 Inventory";
			this.Browse_F41_3.UseVisualStyleBackColor = true;
			// 
			// Browse_F41_2
			// 
			this.Browse_F41_2.Location = new System.Drawing.Point(172, 49);
			this.Browse_F41_2.Name = "Browse_F41_2";
			this.Browse_F41_2.Size = new System.Drawing.Size(160, 23);
			this.Browse_F41_2.TabIndex = 5;
			this.Browse_F41_2.Text = "Browse Schedule B-1.1";
			this.Browse_F41_2.UseVisualStyleBackColor = true;
			// 
			// Browse_F41_1
			// 
			this.Browse_F41_1.Location = new System.Drawing.Point(6, 49);
			this.Browse_F41_1.Name = "Browse_F41_1";
			this.Browse_F41_1.Size = new System.Drawing.Size(160, 23);
			this.Browse_F41_1.TabIndex = 4;
			this.Browse_F41_1.Text = "Browse Schedule B-1";
			this.Browse_F41_1.UseVisualStyleBackColor = true;
			// 
			// Browse_DB1BTicket
			// 
			this.Browse_DB1BTicket.Location = new System.Drawing.Point(338, 165);
			this.Browse_DB1BTicket.Name = "Browse_DB1BTicket";
			this.Browse_DB1BTicket.Size = new System.Drawing.Size(160, 23);
			this.Browse_DB1BTicket.TabIndex = 3;
			this.Browse_DB1BTicket.Text = "Browse DB1BTicket";
			this.Browse_DB1BTicket.UseVisualStyleBackColor = true;
			// 
			// Browse_DB1BMarket
			// 
			this.Browse_DB1BMarket.Location = new System.Drawing.Point(172, 165);
			this.Browse_DB1BMarket.Name = "Browse_DB1BMarket";
			this.Browse_DB1BMarket.Size = new System.Drawing.Size(160, 23);
			this.Browse_DB1BMarket.TabIndex = 2;
			this.Browse_DB1BMarket.Text = "Browse DB1BMarket";
			this.Browse_DB1BMarket.UseVisualStyleBackColor = true;
			// 
			// DataGridViewPanel
			// 
			this.DataGridViewPanel.Controls.Add(this.ResultDataGridView);
			this.DataGridViewPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.DataGridViewPanel.Location = new System.Drawing.Point(0, 0);
			this.DataGridViewPanel.Name = "DataGridViewPanel";
			this.DataGridViewPanel.Size = new System.Drawing.Size(800, 351);
			this.DataGridViewPanel.TabIndex = 4;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 572);
			this.Controls.Add(this.DataGridViewPanel);
			this.Controls.Add(this.OperationPanel);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "MainForm";
			this.Text = "LINQ";
			((System.ComponentModel.ISupportInitialize)(this.ResultDataGridView)).EndInit();
			this.OperationPanel.ResumeLayout(false);
			this.ActionBox.ResumeLayout(false);
			this.ImportGroupBox.ResumeLayout(false);
			this.DataGridViewPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.bindingSource)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button Browse_DB1BCoupon;
		private System.Windows.Forms.Button Qa;
		private System.Windows.Forms.DataGridView ResultDataGridView;
		private System.Windows.Forms.Panel OperationPanel;
		private System.Windows.Forms.Panel DataGridViewPanel;
		private System.Windows.Forms.BindingSource bindingSource;
		private System.Windows.Forms.Button Browse_DB1BTicket;
		private System.Windows.Forms.Button Browse_DB1BMarket;
		private System.Windows.Forms.GroupBox ImportGroupBox;
		private System.Windows.Forms.Button Browse_F41_3;
		private System.Windows.Forms.Button Browse_F41_2;
		private System.Windows.Forms.Button Browse_F41_1;
		private System.Windows.Forms.Button Browse_F41_6;
		private System.Windows.Forms.Button Browse_F41_5;
		private System.Windows.Forms.Button Browse_F41_4;
		private System.Windows.Forms.Button Browse_F41_12;
		private System.Windows.Forms.Button Browse_F41_11;
		private System.Windows.Forms.Button Browse_F41_10;
		private System.Windows.Forms.Button Browse_F41_9;
		private System.Windows.Forms.Button Browse_F41_8;
		private System.Windows.Forms.Button Browse_F41_7;
		private System.Windows.Forms.Button Qh;
		private System.Windows.Forms.Button Qg;
		private System.Windows.Forms.Button Qf;
		private System.Windows.Forms.Button Qe;
		private System.Windows.Forms.Button Qd;
		private System.Windows.Forms.Button Qc;
		private System.Windows.Forms.Button Qb;
		private System.Windows.Forms.GroupBox ActionBox;
	}
}

