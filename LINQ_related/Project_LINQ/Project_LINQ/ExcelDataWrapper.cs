﻿using LinqToExcel.Attributes;
using Microsoft.EntityFrameworkCore;
using Remotion.Logging;
using System.Reflection;
using System.Text;


namespace Project_LINQ
{
	public class ExcelDataWrapper
	{
		public string ItinID { get; set; }
		public string MktID { get; set; }
		public string SeqNum { get; set; }
		public string Coupons { get; set; }
		public int Year { get; set; }
		public string OriginAirportID { get; set; }
		public string OriginAirportSeqID { get; set; }
		public string OriginCityMarketID { get; set; }
		public string Quarter { get; set; }
		public string Origin { get; set; }
		public string OriginCountry { get; set; }
		public string OriginStateFips { get; set; }
		public string OriginState { get; set; }
		public string OriginStateName { get; set; }
		public string OriginWac { get; set; }
		public string DestAirportID { get; set; }
		public string DestAirportSeqID { get; set; }
		public string DestCityMarketID { get; set; }
		public string Dest { get; set; }
		public string DestCountry { get; set; }
		public string DestStateFips { get; set; }
		public string DestState { get; set; }
		public string DestStateName { get; set; }
		public string DestWac { get; set; }
		public string Break { get; set; }
		public string CouponType { get; set; }
		public string TkCarrier { get; set; }
		public string OpCarrier { get; set; }
		public string RPCarrier { get; set; }
		public string Passengers { get; set; }
		public string FareClass { get; set; }
		public string Distance { get; set; }
		public string DistanceGroup { get; set; }
		public string Gateway { get; set; }








		#region other
		private PropertyInfo[] _PropertyInfos = null;

		public override string ToString()
		{
			if (_PropertyInfos == null)
				_PropertyInfos = this.GetType().GetProperties();

			var sb = new StringBuilder();

			foreach (var info in _PropertyInfos)
			{
				var value = info.GetValue(this, null) ?? "(null)";
				sb.AppendLine(info.Name + ": " + value.ToString());
			}

			return sb.ToString();
		}
		#endregion
	}
}
