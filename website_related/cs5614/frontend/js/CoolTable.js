function NodeIndex(el) {
    var i=0;
    while(el.previousElementSibling ) {
        el=el.previousElementSibling;
        i++;
    }
    return i;
}
function GetTable() {return document.getElementById("CoolTable");}
function GetHead(){return GetTable().querySelector("thead");}
function GetBody(){return GetTable().querySelector("tbody");}
function GetRowCount()
{
    return GetBody().querySelectorAll("tr").length + 1;
}
function GetColCount()
{
    return GetHead().querySelectorAll("th").length;
}

function GetCellsOnRow(element)
{
    return findParentBySelector(element, "tr").querySelectorAll("th");
}


function Toggle(element)
{
    element.classList.toggle("bg-warning");
}

function ToggleRow(element)
{
    // alert("1");
    var DesireState = !element.classList.contains("bg-warning");
    GetCellsOnRow(element).forEach(
        current=>{
            if (current == element)
            {
                Toggle(current);
            }
            else
            {
                index = NodeIndex(current);
                var CurrentColHeader = GetHead().querySelectorAll("th")[index].classList.contains("bg-warning");
                if (!CurrentColHeader)
                {
                    if (DesireState && current.classList.contains("bg-warning"))
                    {

                    }
                    else
                    {
                        Toggle(current);
                    }
                }
            }
        });
}

function ClearAll(element)
{
    GetTable().querySelectorAll("th").forEach(
            current=>clear(current)
    );

    document.getElementById("extraHeader").querySelectorAll("th").forEach(
        current=>clear(current)
    );

    function clear(current)
    {
        if (current.classList.contains("bg-warning"))
        {
            Toggle(current);
        }
    }
}

function push(list, items) {
    Array.prototype.push.apply(list, items);
    list.length_ = list.length;
    list.length_ += items.length;
}


function ToggleCol(element)
{
    index = NodeIndex(element);
    var DesireState = !GetHead().querySelectorAll("th")[index].classList.contains("bg-warning");

    var array =GetTable().querySelectorAll("tr");
    // push(array, document.getElementById("extraHeader"));
    array.forEach(
        current=>CoolRowTask(current, element, DesireState)
    );
    CoolRowTask(document.getElementById("extraHeader"), element, DesireState);
    Toggle(element);

    function CoolRowTask(row, element, DesireState)
    {
        current = row.querySelectorAll("th")[index];
        if (current == element)
        {

        }
        else
        {
            var CurrentRowHeader = GetCellsOnRow(current)[0].classList.contains("bg-warning");
            if (!CurrentRowHeader)
            {
                if (DesireState && current.classList.contains("bg-warning"))
                {

                }
                else
                {
                    Toggle(current);
                }

            }
        }
    }
}



function ToggleCell(element)
{
    index = NodeIndex(element);
    state = findParentBySelector(element, "tr").querySelectorAll("th")[0].classList.contains("bg-warning");
    state = state || GetHead().querySelectorAll("tr")[0].querySelectorAll("th")[index].classList.contains("bg-warning");
    if (!state) {
        element.classList.toggle("bg-warning");
    }
}