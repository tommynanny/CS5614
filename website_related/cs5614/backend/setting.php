<?php
//header("Access-Control-Allow-Origin: *");
#this is the setting file for different server host
define('BASE_FILE_PATH', dirname(__FILE__) . '/..'); #app folder location
define('WEB_Name', 'cs5614');

$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 5)) == 'https' ? 'https' : 'http';
$protocol = isset($_SERVER["HTTPS"]) ? 'https://' : 'http://';

if ($_SERVER['SERVER_NAME'] === "localhost") {
    //local url is the same as base file path
    define('BASE_URL', $protocol . 'localhost/tommynanny/websites/' . WEB_Name);
    define('BASE_URL_NO_WEB', $protocol . 'localhost/tommynanny/' . WEB_Name);
    $isRunningOnRemoteServer = false;
} else {
    //remote
    define('BASE_URL', $protocol . 'tommynanny.com/websites/' . WEB_Name);
    define('BASE_URL_NO_WEB', $protocol . 'tommynanny.com/' . WEB_Name);
    $isRunningOnRemoteServer = true;
}

define("TheLastHope_Default_Version", "2020.02.24");

define('JS_PATH', BASE_FILE_PATH . '/frontend/js');
define('IMG_PATH', BASE_FILE_PATH . '/frontend/img');
define('CSS_PATH', BASE_FILE_PATH . '/frontend/css');
define('PLUGINS_PATH', BASE_FILE_PATH . '/backend/plugins');
define('VIEWS_PATH', BASE_FILE_PATH . '/backend/views');
define('HELPER_PATH', BASE_FILE_PATH . '/backend/helper');

define('JS_URL', BASE_URL . '/frontend/js');
define('IMG_URL', BASE_URL . '/frontend/img');
define('CSS_URL', BASE_URL . '/frontend/css');
define('FONT_URL', BASE_URL . '/frontend/font');
define('PLUGINS_URL', BASE_URL . '/backend/plugins');
define('VIEWS_URL', BASE_URL . '/backend/views');
define('EXTENSION_URL', BASE_URL . '/backend/extensions');
define('GROUP_NAME', 'Anonymous');

define('USE_PASS_WORD', false);
define('USE_IP_BLACKLIST', true);

include_once 'credentials.php';

