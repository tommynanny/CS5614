<?php
include_once VIEWS_PATH . "/ResourceDB/DB1BData.php";
include_once VIEWS_PATH . "/ResourceDB/F41Data.php";

$attributesMap = array();
$tablesMap = array();
$IndexAttributeMap = array();
$IndexTableMap = array();

function Label($input, $color = "blue")
{
    $colors = array(
        "blue" => "primary",
        "gray" => "secondary",
        "green" => "success",
        "red" => "danger",
        "yellow" => "warning",
        "cyan" => "info"
    );
    $colorResult = array_key_exists($color, $colors) ? $colors[$color] : $colors["blue"];
    return "<span class='badge badge-" . $colorResult . "'>" . $input . "</span>";
}

function ProcessDataTable($rawDataLines, &$attributesMap, $writeMode, $dataTableName)
{
    $result = "";
    $arr = explode("\n", $rawDataLines);

    foreach ($arr as $line) {

        if (StrContains(substr($line, 0, strlen("//")), "//")) {
            $sectionTitle = substr($line, strlen("//"));
            $result = $result . " <hr><li><b><a style='color: darkgreen'>" . $sectionTitle . "</a></b></li>\n";
            continue;
        }

        $fieldName = preg_split('/\s+/', $line)[0];

        if ($writeMode) {
            if (!is_null($attributesMap)) {
                //add to map
                if (array_key_exists($fieldName, $attributesMap)) {
                    array_push($attributesMap[$fieldName], $dataTableName);
                } else {
                    $attributesMap[$fieldName] = array($dataTableName);
                }
            }
        } else {
            if (!is_null($attributesMap)) {
                $detail = substr($line, strlen($fieldName));

                //trimming the "analysis"
                if (StrContains(substr($detail, -strlen("Analysis") - 1), "Analysis")) {
                    $detail = substr($detail, 0, -strlen("Analysis") - 1);
                }
                $count = GetFieldInstance($fieldName, $attributesMap);
                $color = $count == 1 ? "black" : "orange";
                $countText = $count == 1 ? "" : " <a style='font-size: 10px; color: blue'>[" . count($attributesMap[$fieldName]) . "]</a>";
                //rendering
                $result = $result . "<li>&bull; <b><a style='color: " . $color . "'>" . $fieldName . $countText . "</a></b> " . $detail . "</li>\n";
            }
        }

    }
    return $result;
}


function ProcessDataBase($rawDataBase, &$attributeMap, $writeMode = true, $databaseName = "", &$tableMap = null)
{
    $treeResult = "<ul class='MyTreeView'>
                    <li><span class='caret'>" . $databaseName . " " . Label("database", "blue") . "</span>
                        <ul class='nested' >";

    foreach ($rawDataBase as $tableName => $rawTable) {
        $weight = $writeMode ? "" : " <a style='font-size: 10px; color: red'>[" . GetTotalWeightForTable($tableName, $tableMap, $attributeMap) . "]</a>";
        $treeResult = $treeResult .
            "<li><span class='caret'>" . $tableName . $weight . " " . Label("Table", "cyan") . "</span>
                                    <ul class='nested'>
                                        " . ProcessDataTable($rawTable, $attributeMap, $writeMode, $tableName) . "
                                    </ul>
                                </li>";
    }


    $treeResult = $treeResult . "</ul></li></ul>";
    return $treeResult;
}

function SetupTableMap($attributeMap, &$tableMap)
{
    if (is_null($attributeMap) || is_null($tableMap)) return;
    foreach ($attributeMap as $fieldName => $TableNames) {
        foreach ($TableNames as $TableName) {
            if (array_key_exists($TableName, $tableMap)) {
                array_push($tableMap[$TableName], $fieldName);
            } else {
                $tableMap[$TableName] = array($fieldName);
            }
        }
    }
}

function GetTotalWeightForTable($tableName, $tableMap, $attributeMap)
{
    $result = 0;
    if (is_null($attributeMap) || is_null($tableMap)) return 0;
    if (!array_key_exists($tableName, $tableMap)) return 0;
    foreach ($tableMap[$tableName] as $fieldName) {
        $needAdd = GetFieldInstance($fieldName, $attributeMap);
        $result = $result + ($needAdd < 2 ? 0 : $needAdd);
    }
    return $result;
}

function GetFieldInstance($fieldName, $attributeTable)
{
    if (is_null($attributeTable)) return 0;
    if (!array_key_exists($fieldName, $attributeTable)) return 0;
    return count($attributeTable[$fieldName]);
}

function BuildAttributeIndexMap($map, &$indexMap)
{
    foreach ($map as $k => $v) {
        array_push($indexMap, $k);
    }
    $size = sizeof($map);

    //sorting
    for ($x = 0; $x < $size - 1; $x++) {
        for ($y = 0; $y < $size - 1; $y++) {
            if (GetFieldInstance($indexMap[$y], $map) <= GetFieldInstance($indexMap[$y + 1], $map)) {
                $temp = $indexMap[$y + 1];
                $indexMap[$y + 1] = $indexMap[$y];
                $indexMap[$y] = $temp;
            }
        }
    }
}

function BuildTableIndexMap($map, $AttributeMap, &$indexMap)
{
    foreach ($map as $k => $v) {
        array_push($indexMap, $k);
    }
    $size = sizeof($map);

    //sorting
    for ($x = 0; $x < $size - 1; $x++) {
        for ($y = 0; $y < $size - 1; $y++) {
            if (GetTotalWeightForTable($indexMap[$y], $map, $AttributeMap) <= GetTotalWeightForTable($indexMap[$y + 1], $map, $AttributeMap)) {
                $temp = $indexMap[$y + 1];
                $indexMap[$y + 1] = $indexMap[$y];
                $indexMap[$y] = $temp;
            }
        }
    }
}

function SortAttributesMapTableValues(&$AttributeMap, $tableIndex)
{
    foreach ($AttributeMap as $field => $tables) {
        $size = count($tables);
        //sorting
        for ($x = 0; $x < $size - 1; $x++) {
            for ($y = 0; $y < $size - 1; $y++) {
                if (array_search($tables[$y], $tableIndex) >= array_search($tables[$y + 1], $tableIndex)) {
                    $temp = $tables[$y + 1];
                    $tables[$y + 1] = $tables[$y];
                    $tables[$y] = $temp;
                }
            }
        }
        $AttributeMap[$field] = $tables;
    }
}

function SortTablesMapAttributesValues(&$tableMap, $attributeIndex)
{
    foreach ($tableMap as $table => $attributes) {
        $size = count($attributes);
        //sorting
        for ($x = 0; $x < $size - 1; $x++) {
            for ($y = 0; $y < $size - 1; $y++) {
                if (array_search($attributes[$y], $attributeIndex) >= array_search($attributes[$y + 1], $attributeIndex)) {
                    $temp = $attributes[$y + 1];
                    $attributes[$y + 1] = $attributes[$y];
                    $attributes[$y] = $temp;
                }
            }
        }
        $tableMap[$table] = $attributes;
    }
}


function DebugAttributeMapWithIndex($map, $indexMap)
{
    $result = "<div class = 'col-12'>";
    if (sizeof($map) != count($indexMap)) {
        $result = $result . "<a>size not match!</a><br>";
    }
    $size = count($indexMap);
    for ($x = 0; $x < $size - 1; $x++) {
        $key = $indexMap[$x];
        $result = $result . "<a>" . "<b><a style='color: red'>[" . GetFieldInstance($key, $map) . "]</a></b> <a style='color: blue'>" . $key . "</a>  ====>  <a style='overflow-wrap: anywhere;'>" . json_encode($map[$key]) . "</a></a><br>";
    }
    $result = $result . "</div>";
    return $result;
}

function DebugTableMapWithIndex($map, $AttributeMap, $indexMap)
{
    $result = "<div class = 'col-12'>";
    if (sizeof($map) != count($indexMap)) {
        $result = $result . "<a>size not match!</a><br>";
    }
    $size = count($indexMap);
    for ($x = 0; $x < $size - 1; $x++) {
        $key = $indexMap[$x];
        $result = $result . "<a>" . "<b><a style='color: #ff0000'>[" . GetTotalWeightForTable($key, $map, $AttributeMap) . "]</a></b> <a style='color: #0000ff'>" . $key . "</a>  ====>  <a style='overflow-wrap: anywhere;'>" . json_encode($map[$key]) . "</a></a><br>";
    }
    $result = $result . "</div>";
    return $result;
}

function GetColHeadersRow($IndexTableMap, $tableMap, $attributeMap, $isRaw = false)
{
    $result = "";
    $color = $isRaw ? 'black' : 'yellow';

    foreach ($IndexTableMap as $tableName) {
        $weight = "[" . GetTotalWeightForTable($tableName, $tableMap, $attributeMap) . "]";
//        $weight = $isRaw ?"<br>" .$weight."<br>": $weight;
        $weight = "<br>" . $weight . "<br>";

        $tableName = str_replace("Schedule ", "Schedule<br>", $tableName);
        $tableName = str_replace("DB1B", "DB1B<br>", $tableName);
        $tableName = str_replace(" ", "<br>", $tableName);
        $result = $result . "<th scope='col' style='font-size:10px' onclick='ToggleCol(this)'>" . " <a style='color: " . $color . ";'>" . $weight . "</a> " . $tableName . "</th>";
    }

    return $result;
}

function GetRowByAttributeName($attributeName, $IndexTableMap, $tableMap, $attributeMap, $isRaw = false)
{
    $color1 = $isRaw ? 'black' : 'yellow';
    $result = "<tr'><th scope='row' style='font-size:12px' onclick='ToggleRow(this)'>  <a class='noselect'><label style='color: " . $color1 . "'>[" . GetFieldInstance($attributeName, $attributeMap) . "]</label>" . $attributeName . "</a></th>";
    foreach ($IndexTableMap as $tableName) {
        if (in_array($tableName, $attributeMap[$attributeName])) {
            $color2 = $isRaw ? 'black' : 'cyan';
            $checked = $isRaw ? 'v' : "<i class='fas fa-check'></i>";
            $result = $result . "<th scope='col' style='font-size:12px' onclick='ToggleCell(this)'>" . " <a style='color: " . $color2 . "'>" . $checked . "</a></th>";
        } else {
            $color2 = $isRaw ? 'black' : 'red';
            $checked = $isRaw ? 'x' : "<i class='fas fa-times'></i>";
            $result = $result . "<th scope='col' style='font-size:12px' onclick='ToggleCell(this)'>" . " <a style='color: " . $color2 . "'>" . $checked . "</a></th>";
        }
    }
    $result = $result . "</tr>";
    return $result;
}

function GetRows($IndexAttributeMap, $IndexTableMap, $tableMap, $attributeMap, $isRaw = false)
{
    $result = "";
    foreach ($IndexAttributeMap as $AttributeName) {
        $result = $result . GetRowByAttributeName($AttributeName, $IndexTableMap, $tableMap, $attributeMap, $isRaw);
    }
    return $result;
}

//run the task here
ProcessDataBase($DB1B, $attributesMap, true);
ProcessDataBase($F41D, $attributesMap, true);

SetupTableMap($attributesMap, $tablesMap);

BuildAttributeIndexMap($attributesMap, $IndexAttributeMap);
BuildTableIndexMap($tablesMap, $attributesMap, $IndexTableMap);

SortAttributesMapTableValues($attributesMap, $IndexTableMap);
SortTablesMapAttributesValues($tablesMap, $IndexAttributeMap);
?>