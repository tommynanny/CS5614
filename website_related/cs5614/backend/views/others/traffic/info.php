<link rel="icon" href="<?= IMG_URL ?>/fav.png" type="image/icon type">
<!-- Bootstrap core CSS -->
<link href="<?= CSS_URL ?>/bootstrap.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/1.0.0/mdb.min.css" rel="stylesheet"/>
<!-- Custom styles for this template -->
<link href="<?= CSS_URL ?>/custom.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
<script type="text/javascript" src="<?= JS_URL ?>/CoolTable.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/1.0.0/mdb.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
      integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

<p style="font-size: 15px">As part of our SQL and PHP project study, we do collect information relating to traffic on
    current websites
    ("Traffic Data"). Specifically, on every connection with the current website, <b>ONLY</b> visitor's IP addresses are
    recorded into the SQL visitor database; Your connection IP is recorded every time you visit our website which
    includes
    a refresh on the same page; we do not collect any personal data other than IP addresses from visitors which are to
    show geolocation information
    as part of SQL data visualization practice. If you don't want to share your connection record, you can
    manually remove the latest record associated with your current IP addresses' connection from the visitor
    database by clicking the button below.

    <?php
    $countLeft = GetCountOfConnection();
    $w = $countLeft == 1 ? "is" : "are";
    $r = $countLeft == 1 ? "record" : "records";
    if ($countLeft > 0):
    ?>
<div class="alert alert-warning" role="alert" id="nav">
    There <?= $w ?> <b style="color: red"><?= $countLeft ?></b> active <?= $r ?> associated with your current IP address
    from our database.
</div>
<?php else: ?>
    <div class="alert alert-success" role="alert" id="nav">
        There are <b>NO</b> active records associated with your current IP address from our database.
    </div>
<?php endif; ?>

<div class="alert alert-info" role="alert" id="nav">
    <div class="col-md-12 text-center" style="font-size: 10px">
        <b><?= Getip(); ?><br>
            <?= GetCurrentGEOData(); ?><br></b>
    </div>
</div>
</p>

<div class="col-md-12 text-center">
    <button class="btn btn-danger" onclick="parent.ShowLatestTrafficData();" style="align-self: center">Delete Current
        IP Latest Record
    </button>
</div>

<div id="my_element" class="container clear-top" style="font-size:12px;color:darkgray;">If you still have questions,
    please contact us.
</div>
