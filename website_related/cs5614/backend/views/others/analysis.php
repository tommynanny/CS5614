<!-- Begin page content -->
<main role="main" class="container">
    <div class="row">

        <div id="analysis_nav"></div>

        <?php

        include_once VIEWS_PATH . "/ResourceDB/ResourceDBProcessor.php";

        //        echo DebugAttributeMapWithIndex($attributesMap, $IndexAttributeMap);
        ?>
        <section id="analysis" class="mt-5">
            <h3 class="display-5"><i class="fas fa-search-dollar"></i></i> Analysis</h3>
            <hr>

            <div class="alert alert-primary" role="alert">
                <ul>
                    <li> &bull; The number next to the name of the attribute is the <b><i>attribute weight</b></i>, indicating the number of occurrences of current attribute in all 15  data tables.</li>
                    <li> &bull; The number next to the name of the data table is the <b><i>data table weight</b></i>, summing all its attributes' <b><i>attribute weights</b></i> <u>that are greater than 1</u>.</li>
                    <li> &bull; Both <b><i>weights</b></i> are gathered in 2 databases, among all 15 data tables, 3 from <u>DB1B Airline Data</u> and 12 from <u>Form 41 Financial Data</u>.</li>
                    <li> &bull; The <b><i>weight</b></i> shows how relative a data table or an attribute is in all data tables or attributes from these 2 databases.</li>
                    <li> &bull; We are interested in data tables and attributes with high <b><i>weight</b></i>.</li>
                </ul>
            </div>



            <ul class="timelinelist">
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header">
                            <h2 class="mb-0">
                                <a type="button" class="btn" data-toggle="collapse"
                                   data-target="#collapseOne" data-toggle="tooltip" data-placement="left" title="click to expand the structure tree">

                                    <i class="far fa-hand-pointer fa-2x"></i>

                                    <a style="margin-left:100px; font-size: 15px"
                                       href="https://www.transtats.bts.gov/DatabaseInfo.asp?DB_ID=125"
                                       target="_blank">DB1B Airline Data</a>

                                    <a style="font-size: 15px">
                                                <span class="badge badge-primary">
                                                    Database
                                                </span>
                                        (Airline Origin and Destination Survey)
                                    </a>

                            </h2>
                        </div>
                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <?= ProcessDataBase($DB1B, $attributesMap, false, "DB1B Airline Data", $tablesMap) ?>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2 class="mb-0">
                                <a type="button" class="btn" data-toggle="collapse"
                                   data-target="#collapseTwo" data-placement="left" title="click to expand structure tree">
                                    <i class="far fa-hand-pointer fa-2x"></i>
                                    <a style="margin-left:100px; font-size: 15px"
                                       href="https://www.transtats.bts.gov/DatabaseInfo.asp?DB_ID=135"
                                       target="_blank">Form 41 Financial Data</a>
                                    <a style="font-size: 15px">
                                                <span class="badge badge-primary">
                                                    DataBase
                                                </span>
                                        (Air Carrier Financial Reports)
                                    </a>
                                </a>
                            </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingOne"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <?= ProcessDataBase($F41D, $attributesMap, false, "Form 41 Financial Data", $tablesMap); ?>
                            </div>
                        </div>
                    </div>

                </div>
            </ul>
        </section>


        <br>
        <hr width="100%" size="20" align="center">

        <div class="btn-group" role="group" style="background:none; border:none; box-shadow:none;">
            <button class="btn btn-warning ripple" onclick="ShowSortedTable()" >
                <i class="fas fa-sort-amount-down"></i> Tables sorted by Weight
            </button>
            <button class="btn btn-success ripple" onclick="ShowSortedAttributes()">
                <i class="fas fa-sort-amount-down"></i> Attributes sorted by Weight
            </button>
            <button class="btn btn-dark ripple" onclick="ShowAttributeTableGraph()" >
                <i class="fas fa-table"></i> Attributes-Tables Relation Table
            </button>
            <button class="btn btn-danger ripple" id="HideAllButton" onclick="HideAll()" style="visibility: hidden">
                <i class="fas fa-chevron-circle-up"></i> Hide
            </button>
        </div>
        <br>
        <hr width="100%" size="20" align="center">

        <div id="sortedArea">

            <div id="SortedTable" style="display: none" class="alert alert-warning" role="alert">
                <div class="alert alert-primary" role="alert">
                    Notice that the attribute set for each of the data table is also sorted by its items' attribute weights.
                </div>
                <?= DebugTableMapWithIndex($tablesMap, $attributesMap, $IndexTableMap); ?>
            </div>

            <div id="SortedAttributes" style="display: none" class="alert alert-success" role="alert">
                <div class="alert alert-primary" role="alert">
                    Notice that the data table set for each of the attribute is also sorted by its items' table weights.
                </div>
                <?= DebugAttributeMapWithIndex($attributesMap, $IndexAttributeMap); ?>
            </div>

            <div id="AttributeTableGraph" style="display: none" class="alert alert-dark" role="alert">

                <a id="tracker"></a>
                <!--                floating header-->
                <div id="header-inner" style=" position: sticky; top : 50px; display: none"> <!-- Note #1 -->
                    <table class="table table-sm table-dark">
                        <thead>
                        <tr id="extraHeader">
                            <th scope="col">
                                <a style="visibility: hidden">aaaaa</a>
                                <button class="btn btn-danger ripple" id="HideAllButton" onclick="ClearAll(this)" >
                                    <i class="fas fa-eraser"></i> Clear Highlight
                                </button>
                            </th>
                            <?php
                            echo GetColHeadersRow($IndexTableMap, $tablesMap, $attributesMap);
                            ?>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope='row'>

                            </tr>
                        </tbody>
                    </table>
                </div>


                <div id="content">

                    <div class="alert alert-primary" role="alert">
                        You can click to highlight row/column/cell
                    </div>

                    <table class="table table-sm table-dark" id="CoolTable">
                        <thead>
                        <tr>
                            <th scope="col">
                                <a style="visibility: hidden">aaaaa</a>
                                <button class="btn btn-danger ripple" id="HideAllButton" onclick="ClearAll(this)" >
                                    <i class="fas fa-eraser"></i> Clear Highlight
                                </button>
                            </th>
                            <?php
                            echo GetColHeadersRow($IndexTableMap, $tablesMap, $attributesMap);
                            ?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        echo GetRows($IndexAttributeMap, $IndexTableMap, $tablesMap, $attributesMap);
                        ?>
                        </tbody>
                    </table>


                    <!-- Some Content Here -->
                </div>
            </div>

        </div>
        <!--        debug section-->

        <script>
            function ShowSortedTable() {
                if (document.getElementById("SortedTable").style.display != "none") {
                    HideAll();
                    return;
                }

                document.getElementById("SortedAttributes").style.display = "none";
                document.getElementById("SortedTable").style.display = "block";
                document.getElementById("AttributeTableGraph").style.display = "none";

                document.getElementById("HideAllButton").style.visibility = "visible";
            }

            function ShowSortedAttributes() {
                if (document.getElementById("SortedAttributes").style.display != "none") {
                    HideAll();
                    return;
                }

                document.getElementById("SortedAttributes").style.display = "block";
                document.getElementById("SortedTable").style.display = "none";
                document.getElementById("AttributeTableGraph").style.display = "none";

                document.getElementById("HideAllButton").style.visibility = "visible";
            }

            function ShowAttributeTableGraph() {
                if (document.getElementById("AttributeTableGraph").style.display != "none") {
                    HideAll();
                    return;
                }

                document.getElementById("SortedAttributes").style.display = "none";
                document.getElementById("SortedTable").style.display = "none";
                document.getElementById("AttributeTableGraph").style.display = "block";

                document.getElementById("HideAllButton").style.visibility = "visible";
            }


            function HideAll() {
                document.getElementById("SortedAttributes").style.display = "none";
                document.getElementById("SortedTable").style.display = "none";
                document.getElementById("AttributeTableGraph").style.display = "none";

                document.getElementById("HideAllButton").style.visibility = "hidden";
            }
        </script>

</main>



