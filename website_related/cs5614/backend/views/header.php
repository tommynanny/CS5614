<!DOCTYPE html>

<!--https://zenodo.org/record/3987216#.X3QnMHlKiiM-->

<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?= $pageTitle ?></title>
    <link rel="icon" href="<?= IMG_URL ?>/fav.png" type="image/icon type">
    <!-- Bootstrap core CSS -->
    <link href="<?= CSS_URL ?>/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/1.0.0/mdb.min.css" rel="stylesheet"/>
    <!-- Custom styles for this template -->
    <link href="<?= CSS_URL ?>/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
    <script type="text/javascript" src="<?= JS_URL ?>/CoolTable.js"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/1.0.0/mdb.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>

<script type="text/javascript">
    function logoutFunction() {
        delete_cookie("verify");
        window.location.href = "<?=BASE_URL_NO_WEB?>";
    }

    function SwitchPhoto(element) {
        var link = element.getElementsByTagName("img")[0].src;
        if (link.includes("png")) {
            link = link.replace("png", "jpg");
        } else if (link.includes("jpg")) {
            link = link.replace("jpg", "png");
        }
        element.getElementsByTagName("img")[0].src = link;
    }

    function collectionHas(a, b) { //helper function (see below)
        for (var i = 0, len = a.length; i < len; i++) {
            if (a[i] == b) return true;
        }
        return false;
    }

    function findParentBySelector(elm, selector) {
        var all = document.querySelectorAll(selector);
        var cur = elm.parentNode;
        while (cur && !collectionHas(all, cur)) { //keep going up until you find a match
            cur = cur.parentNode; //go up
        }
        return cur; //will return null if not found
    }


    function OpenInNewPage() {
        var url = window.location.href;
        var win = window.open(url, '_blank');
        win.focus();
    }

    function delete_cookie(name) {
        document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }

    function goto(id) {
        document.getElementById(id).scrollIntoView({block: "center"});
    }


</script>


<!-- header -->
<header>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md fixed-top bg-dark bg-vt">
        <div class="container">
            <a class="navbar-brand" href="https://www.vt.edu/">
                <img src="<?= IMG_URL ?>/vt.png" width="200" height="30" alt="vt">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i></button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">

                    </li>
                    <a class="nav-link" href="<?= BASE_URL_NO_WEB ?>/">Main</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="<?= BASE_URL_NO_WEB ?>/analysis">Analysis</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="<?= BASE_URL_NO_WEB ?>/timeline">Timeline</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" style="color:#bdfffc" href="<?= BASE_URL_NO_WEB ?>/traffic">Traffic</a>
                    </li>


                    <li class="nav-item">
                        <a class="nav-link" style="color:#d1d1d1" href="<?= BASE_URL_NO_WEB ?>/demo">Demo</a>
                    </li>

                </ul>
            </div>
            <?php if (USE_PASS_WORD): ?>
                <button class="btn btn-secondary" onclick="logoutFunction()"><i class="fas fa-sign-out-alt"></i> Logout
                </button>
            <?php elseif (isset($_SERVER['HTTP_SEC_FETCH_DEST']) && $_SERVER['HTTP_SEC_FETCH_DEST'] == 'iframe'): ?>
                <button class="btn btn-primary" onclick="OpenInNewPage()"><i
                            class="fas fa-external-link-square-alt"></i> Full Screen
                </button>
            <?php endif; ?>
        </div>
    </nav>
</header>

<body>

